<?php

namespace Thunderwolf\EloquentSortable;

use Illuminate\Database\Schema\Blueprint;

class SortableBlueprint
{
    /**
     * Add sluggable column to the table. Also create an index.
     *
     * @param Blueprint $table
     * @param array $columns
     * @throws SortableException
     */
    public static function createSortableColumns(Blueprint $table, array $columns)
    {
        $columns = SortableHelper::parseColumns($columns);
        $table->unsignedInteger($columns['rank_column'])->default(0);
        if ($columns['use_scope']) {
            $index = $columns['scope_columns'];
            $index[] = $columns['rank_column'];
            $table->index($index);
        } else {
            $table->index([$columns['rank_column']]);
        }
    }

    /**
     * Drop sluggable column and index.
     *
     * @param Blueprint $table
     * @param array $columns
     * @throws SortableException
     */
    public static function dropSortableColumns(Blueprint $table, array $columns)
    {
        $columns = SortableHelper::parseColumns($columns);
        if ($columns['use_scope']) {
            $index = $columns['scope_columns'];
            $index[] = $columns['rank_column'];
            $table->dropIndex($index);
        } else {
            $table->dropIndex([$columns['rank_column']]);
        }
        $table->dropColumn($columns['rank_column']);
    }

}