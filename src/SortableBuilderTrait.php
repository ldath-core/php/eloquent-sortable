<?php

namespace Thunderwolf\EloquentSortable;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

trait SortableBuilderTrait
{
    // sortable behavior

    /**
     * Returns the objects in a certain list, from the list scope
     *
     * @param array $scopes Scope to determine which objects node to return
     * @return self The current query, for fluid interface
     */
    public function inList(array $scopes): self
    {
        $that = $this;
        foreach ($scopes as $scopeName => $scopeValue) {
            $that = $that->where($scopeName, $scopeValue);
        }
        return $that;
    }

    /**
     * Filter the query based on a rank in the list and potential scope
     *
     * @param int $rank
     * @param array $scopes Scopes to determine which objects node to return
     * @return self The current query, for fluid interface
     * @throws SortableException
     */
    public function filterByRank(int $rank, array $scopes = []): self
    {
        if (!empty($scopes)) {
            return $this
                ->inList($scopes)
                ->where($this->model->getSortableRankName(), $rank);
        }
        return $this
            ->where($this->model->getSortableRankName(), $rank);
    }

    /**
     * Order the query based on the rank in the list.
     * Using the default $order, returns the item with the lowest rank first
     *
     * @param string $order either asc (default) or desc
     * @return SortableBuilder
     * @throws SortableException
     */
    public function orderByRank(string $order = 'asc'): self
    {
        $order = strtoupper($order);
        switch ($order) {
            case 'ASC':
                return $this->orderBy($this->model->getSortableRankName());
            case 'DESC':
                return $this->orderByDesc($this->model->getSortableRankName());
            default:
                throw new SortableException('orderByRank() only accepts "asc" or "desc" as argument');
        }
    }

    /**
     * Get an item from the list based on its rank
     *
     * @param int $rank rank
     * @param array $scopes Scopes to determine which objects node to return
     * @return Model
     * @throws SortableException
     */
    public function findOneByRank(int $rank, array $scopes = []): Model
    {
        return $this
            ->filterByRank($rank, $scopes)
            ->first();
    }

    /**
     * Returns the sorted list of objects in scope when scope not null or all objects
     *
     * @param array $scopes Scopes to determine which objects node to return
     * @return Collection|null
     * @throws SortableException
     */
    public function findList(array $scopes = []): ?Collection
    {
        if (!empty($scopes)) {
            return $this
                ->inList($scopes)
                ->orderByRank()
                ->get();
        }
        return $this
            ->orderByRank()
            ->get();
    }

    /**
     * Return the number of sortable objects in the given scope or all objects count when scope is null
     *
     * @param array $scopes Scope to determine which objects node to return
     * @return int
     */
    public function countList(array $scopes = []): int
    {
        if (!is_null($scopes)) {
            return $this
                ->inList($scopes)
                ->count();
        }
        return $this
            ->count();
    }

    /**
     *  Deletes the sortable objects in the given scope or all objects when scope is null
     *
     * @param array $scopes Scopes to determine which objects node to return
     * @return int number of deleted objects
     */
    public function deleteList(array $scopes = []): int
    {
        if (!is_null($scopes)) {
            return $this
                ->inList($scopes)
                ->delete();
        }
        return $this
            ->delete();
    }

    /**
     * Get the highest rank
     *
     * @param array $scopes
     * @return int|null
     * @throws SortableException
     */
    public function getMaxRank(array $scopes = []): ?int
    {
        // shift the objects with a position lower than the one of object

        if (!is_null($scopes)) {
            return $this
                ->inList($scopes)
                ->max($this->model->getSortableRankName());
        }
        return $this->max($this->model->getSortableRankName());
    }

    /**
     * Reorder a set of sortable objects based on a list of id/position
     * Beware that there is no check made on the positions passed
     * So incoherent positions will result in an incoherent list
     *
     * @param array $order
     * @return bool true if the reordering took place, false if a database problem prevented it
     */
    public function reorder(array $order): bool
    {
        get_class($this)::getConnection()->beginTransaction();
        try {
            $ids = array_keys($order);
            $objects = $this->findMany($ids);
            foreach ($objects as $object) {
                $pk = $object->getKey();
                if ($object->getSortableRank() != $order[$pk]) {
                    $object->setSortableRank($order[$pk]);
                    $object->save();
                }
            }
            get_class($this)::getConnection()->commit();

            return true;
        } catch (Exception $e) {
            get_class($this)::getConnection()->rollback();
            throw $e;
        }
    }
}