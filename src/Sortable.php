<?php

namespace Thunderwolf\EloquentSortable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

trait Sortable
{
    /**
     * Generated Model configuration cache
     *
     * @var array
     */
    public array $sortableConfiguration = [];

    /**
     * Queries to be executed in the save transaction
     * @var array
     */
    protected array $sortableQueries = [];

    /**
     * The old scope value.
     * @var array
     */
    protected array $oldScope = [];

    /**
     * Configuration for the Sortable Trait
     *
     * @return array Array in the format SortableHelper::COLUMNS
     */
    abstract public static function sortable(): array;

    /**
     * Override -> Create a new Eloquent query builder for the model.
     * If you have more Behaviors using this kind on Override create own and use Trait SortableBuilderTrait
     *
     * @param  Builder  $query
     * @return SortableBuilder
     */
    public function newEloquentBuilder($query): SortableBuilder
    {
        return new SortableBuilder($query);
    }

    /**
     * Booting Eloquent Sortable Trait
     *
     *  creating and created: sent before and after records have been created.
     *  updating and updated: sent before and after records are updated.
     *  saving and saved: sent before and after records are saved (i.e created or updated).
     *  deleting and deleted: sent before and after records are deleted or soft-deleted.
     *  restoring and restored: sent before and after soft-deleted records are restored.
     *  retrieved: sent after records have been retrieved.
     *
     * @return void
     * @throws SortableException
     */
    public static function bootSortable(): void
    {
        static::saving(function (Model $model) {
            $model->processSortableQueries();
        });

        static::creating(function (Model $model) {
            if (!$model->isDirty($model->getSortableRankName())) {
                $model->setSortableRank(get_class($model)::query()->getMaxRank($model->getSortableScopes()) + 1);
            }
        });

        static::updating(function (Model $model) {
            // if scope has changed and rank was not modified (if yes, assuming superior action)
            // insert object to the end of new scope and cleanup old one
            if ($model->isSortableScopeUsed() && $model->isDirty($model->getSortableScopeNames()) && !$model->isDirty($model->getSortableRankName())) {
                self::shiftRank(get_class($model),-1, $this->getSortableRank() + 1, null, $model->oldScope);
                $model->insertAtBottom();
            }
        });

        static::deleting(function (Model $model) {
            self::shiftRank(get_class($model), -1, $model->getSortableRank() + 1, null, $model->getSortableScopes());
        });
    }

    /**
     * Process configuration only once per object
     *
     * @return array
     * @throws SortableException
     */
    public function getSortableConfiguration(): array
    {
        if (empty($this->sortableConfiguration)) {
            $this->sortableConfiguration = SortableHelper::parseColumns(get_called_class()::sortable());
        }
        return $this->sortableConfiguration;
    }

    // accessing methods

    /**
     * Get the [sortable_rank] column key name.
     *
     * @return string
     * @throws SortableException
     */
    public function getSortableRankName(): string
    {
        return $this->getSortableConfiguration()['rank_column'];
    }

    /**
     * Get the [sortable_rank] column value.
     *
     * @return int
     * @throws SortableException
     */
    public function getSortableRank(): int
    {
        return $this->getAttributeValue($this->getSortableRankName());
    }

    /**
     * Set the [sortable_rank] column value.
     *
     * @param int $rank
     * @return void
     * @throws SortableException
     */
    public function setSortableRank(int $rank): void
    {
        $this->setAttribute($this->getSortableRankName(), $rank);
    }

    /**
     * Get the [use_scope] configuration value.
     *
     * @return bool
     * @throws SortableException
     */
    public function isSortableScopeUsed(): bool
    {
        return !!$this->getSortableConfiguration()['use_scope'];
    }

    /**
     * Get the [scope_columns] columns key names.
     *
     * @return array
     * @throws SortableException
     */
    public function getSortableScopeNames(): array
    {
        if (!$this->isSortableScopeUsed()) {
            throw new SortableException('Scope usage is not active but trying to access scope column name');
        }
        return $this->getSortableConfiguration()['scope_columns'];
    }

    /**
     * Get the [scope_column] column value.
     *
     * @param string $key
     * @return int|null
     * @throws SortableException
     */
    public function getSortableScope(string $key): ?int
    {
        if (!$this->isSortableScopeUsed()) {
            return null;
        }
        return $this->getAttributeValue($key);
    }

    /**
     * Get the [scope_columns] column values.
     *
     * @return array
     * @throws SortableException
     */
    public function getSortableScopes(): array
    {
        if (!$this->isSortableScopeUsed()) {
            return [];
        }
        $output = [];
        foreach($this->getSortableScopeNames() as $scopeColumnName) {
            $output[$scopeColumnName] = $this->getAttributeValue($scopeColumnName);
        }
        return $output;
    }

    /**
     * Set the [scope_column] column value.
     *
     * @param string $key
     * @param int $scope
     * @return void
     * @throws SortableException
     */
    public function setSortableScope(string $key, int $scope): void
    {
        if ($this->getSortableScope($key) !== $scope) {
            $this->oldScope[$key] = $this->getSortableScope($key);
            $this->setAttribute($key, $scope);
        }
    }

    // sortable behavior

    /**
     * Check if the object is first in the list, i.e. if it has 1 for rank
     *
     * @return bool
     * @throws SortableException
     */
    public function isFirst(): bool
    {
        return $this->getSortableRank() == 1;
    }

    /**
     * Check if the object is last in the list, i.e. if its rank is the highest rank
     *
     * @return bool
     * @throws SortableException
     */
    public function isLast(): bool
    {
        return $this->getSortableRank() == get_class($this)::query()->getMaxRank($this->getSortableScopes());
    }

    /**
     * Get the next item in the list, i.e. the one for which rank is immediately higher
     *
     * @return Model
     * @throws SortableException
     */
    public function getNext(): Model
    {
        $query = get_class($this)::query();
        $query->filterByRank($this->getSortableRank() + 1, $this->getSortableScopes());
        return $query->first();
    }

    /**
     * Get the previous item in the list, i.e. the one for which rank is immediately lower
     *
     * @return Model
     * @throws SortableException
     */
    public function getPrevious(): Model
    {
        $query = get_class($this)::query();
        $query->filterByRank($this->getSortableRank() - 1, $this->getSortableScopes());
        return $query->first();
    }

    /**
     * Insert at specified rank
     * The modifications are not persisted until the object is saved.
     *
     * @param int $rank rank value
     * @return Model
     * @throws SortableException
     */
    public function insertAtRank(int $rank): Model
    {
        $maxRank = get_class($this)::query()->getMaxRank($this->getSortableScopes());
        if ($rank < 1 || $rank > $maxRank + 1) {
            throw new SortableException('Invalid rank ' . $rank);
        }
        // move the object in the list, at the given rank
        $this->setSortableRank($rank);
        if ($rank != $maxRank + 1) {
            // Keep the list modification query for the save() transaction
            $this->sortableQueries[] = [
                'callable' => array(get_class($this), 'shiftRank'),
                'arguments' => array(get_class($this), 1, $rank, null, $this->getSortableScopes()),
            ];
        }

        return $this;
    }

    /**
     * Insert in the last rank
     * The modifications are not persisted until the object is saved.
     *
     * @return Model the current object
     * @throws SortableException
     */
    public function insertAtBottom(): Model
    {
        $this->setSortableRank(get_class($this)::query()->getMaxRank($this->getSortableScopes()) + 1);
        return $this;
    }

    /**
     * Insert in the first rank
     * The modifications are not persisted until the object is saved.
     *
     * @return Model the current object
     * @throws SortableException
     */
    public function insertAtTop(): Model
    {
        return $this->insertAtRank(1);
    }

    /**
     * Move the object to a new rank, and shifts the rank
     * Of the objects in between the old and new rank accordingly
     *
     * @param int $newRank rank value
     * @return Model the current object
     * @throws SortableException
     */
    public function moveToRank(int $newRank): Model
    {
        if (!$this->exists()) {
            throw new SortableException('New objects cannot be moved. Please use insertAtRank() instead');
        }
        if ($newRank < 1 || $newRank > get_class($this)::query()->getMaxRank($this->getSortableScopes())) {
            throw new SortableException('Invalid rank ' . $newRank);
        }

        $oldRank = $this->getSortableRank();
        if ($oldRank == $newRank) {
            return $this;
        }

        get_class($this)::getConnection()->beginTransaction();
        try {
            // shift the objects between the old and the new rank
            $delta = ($oldRank < $newRank) ? -1 : 1;
            self::shiftRank(get_class($this), $delta, min($oldRank, $newRank), max($oldRank, $newRank), $this->getSortableScopes());

            // move the object to its new rank
            $this->setSortableRank($newRank);
            $this->save();

            get_class($this)::getConnection()->commit();

            return $this;
        } catch (Exception $e) {
            get_class($this)::getConnection()->rollback();
            throw $e;
        }
    }

    /**
     * Exchange the rank of the object with the one passed as argument, and saves both objects
     *
     * @param Model $object the object to swap with
     * @return Model the current object
     * @throws SortableException
     */
    public function swapWith(Model $object): Model
    {
        get_class($this)::getConnection()->beginTransaction();
        try {
            if ($this->isSortableScopeUsed()) {
                foreach($this->getSortableScopeNames() as $scopeColumnName) {
                    $oldScope = $this->getSortableScope($scopeColumnName);
                    $newScope = $object->getSortableScope($scopeColumnName);
                    if ($newScope != $oldScope) {
                        $this->setSortableScope($scopeColumnName, $newScope);
                        $object->setSortableScope($scopeColumnName, $oldScope);
                    }
                }
            }
            $oldRank = $this->getSortableRank();
            $newRank = $object->getSortableRank();
            $this->setSortableRank($newRank);
            $this->save();
            $object->setSortableRank($oldRank);
            $object->save();
            get_class($this)::getConnection()->commit();

            return $this;
        } catch (Exception $e) {
            get_class($this)::getConnection()->rollback();
            throw $e;
        }
    }

    /**
     * Move the object higher in the list, i.e. exchanges its rank with the one of the previous object
     *
     * @return Model
     * @throws SortableException
     */
    public function moveUp(): Model
    {
        if ($this->isFirst()) {
            return $this;
        }
        get_class($this)::getConnection()->beginTransaction();
        try {
            $prev = $this->getPrevious();
            $this->swapWith($prev);
            get_class($this)::getConnection()->commit();

            return $this;
        } catch (Exception $e) {
            get_class($this)::getConnection()->rollback();
            throw $e;
        }
    }

    /**
     * Move the object higher in the list, i.e. exchanges its rank with the one of the next object
     *
     * @return Model
     * @throws SortableException
     */
    public function moveDown(): Model
    {
        if ($this->isLast()) {
            return $this;
        }
        get_class($this)::getConnection()->beginTransaction();
        try {
            $next = $this->getNext();
            $this->swapWith($next);
            get_class($this)::getConnection()->commit();

            return $this;
        } catch (Exception $e) {
            get_class($this)::getConnection()->rollback();
            throw $e;
        }
    }

    /**
     * Move the object to the top of the list
     *
     * @return Model
     * @throws SortableException
     */
    public function moveToTop(): Model
    {
        if ($this->isFirst()) {
            return $this;
        }

        return $this->moveToRank(1);
    }

    /**
     * Move the object to the bottom of the list
     *
     * @return Model
     * @throws SortableException
     */
    public function moveToBottom(): Model
    {
        if ($this->isLast()) {
            return $this;
        }
        get_class($this)::getConnection()->beginTransaction();
        try {
            $bottom = get_class($this)::query()->getMaxRank($this->getSortableScopes());
            $res = $this->moveToRank($bottom);
            get_class($this)::getConnection()->commit();

            return $res;
        } catch (Exception $e) {
            get_class($this)::getConnection()->rollback();
            throw $e;
        }
    }

    /**
     * Removes the current object from the list.
     * The modifications are not persisted until the object is saved.
     *
     * @return Model
     * @throws SortableException
     */
    public function removeFromList(): Model
    {
        if ($this->getSortableScopes() === []) {
            throw new SortableException('Object is already removed (has null scope)');
        }
        // Keep the list modification query for the save() transaction
        $this->sortableQueries[] = [
            'callable' => array(get_class($this), 'shiftRank'),
            'arguments' => array(get_class($this), -1, $this->getSortableRank() + 1, $this->getSortableScopes()),
        ];
        // remove the object from the list
        $this->setSortableRank(null);

        return $this;
    }

    /**
     * Execute queries that were saved to be run inside the save transaction
     */
    protected function processSortableQueries(): void
    {
        foreach ($this->sortableQueries as $query) {
            call_user_func_array($query['callable'], $query['arguments']);
        }
        $this->sortableQueries = array();
    }

    /**
     * Adds $delta to all Rank values that are >= $first and <= $last.
     * '$delta' can also be negative.
     *
     * @param string $origin_class
     * @param int $delta Value to be shifted by, can be negative
     * @param int|null $first First node to be shifted
     * @param int|null $last Last node to be shifted
     * @param array $scope Scope to use for the shift. Array of Scopes values
     * @return mixed
     * @throws SortableException
     */
    public static function shiftRank(string $origin_class, int $delta, ?int $first = null, ?int $last = null, array $scope = []): mixed
    {
        $rank_column = SortableHelper::parseColumns($origin_class::sortable())['rank_column'];
        $use_scope = SortableHelper::parseColumns($origin_class::sortable())['use_scope'];

        $query = $origin_class::query();
        if (null !== $first) {
            $query = $query->where($rank_column, ">=", $first);
        }
        if (null !== $last) {
            $query = $query->where($rank_column, "<=", $last);
        }
        if ($use_scope) {
            if (empty($scope)) {
                throw new SortableException('Scope usage active but empty scope given');
            }
            $scope_columns = SortableHelper::parseColumns($origin_class::sortable())['scope_columns'];
            foreach ($scope_columns as $scope_column) {
                $query = $query->where($scope_column, $scope[$scope_column]);
            }
        }

        return $query->increment($rank_column, $delta);
    }
}