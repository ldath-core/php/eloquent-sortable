<?php

namespace Thunderwolf\EloquentSortable;

class SortableHelper
{
    /**
     * The name of default rank column.
     */
    const RankColumn = 'sortable_rank';

    /**
     * Parsing columns given to
     *
     * @param array $columns
     * @return array
     * @throws SortableException
     */
    public static function parseColumns(array $columns): array
    {
        if (!array_key_exists('rank_column', $columns)) {
            $columns['rank_column'] = self::RankColumn;
        }
        if (array_key_exists('use_scope', $columns) && $columns['use_scope']) {
            if (!array_key_exists('scope_columns', $columns)) {
                throw new SortableException('You can\'t enable scope without setting scope_columns');
            };
        } else {
            $columns['use_scope'] = false;
        }
        return $columns;
    }
}