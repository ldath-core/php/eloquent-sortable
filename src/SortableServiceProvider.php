<?php

namespace Thunderwolf\EloquentSortable;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;

class SortableServiceProvider extends ServiceProvider
{
    public function register()
    {
        Blueprint::macro('createSortable', function (array $columns) {
            SortableBlueprint::createSortableColumns($this, $columns);
        });

        Blueprint::macro('dropSortable', function (array $columns) {
            SortableBlueprint::dropSortableColumns($this, $columns);
        });
    }
}