<?php

namespace Thunderwolf\EloquentSortable;

use Illuminate\Database\Eloquent\Builder;

class SortableBuilder extends Builder
{
    use SortableBuilderTrait;
}