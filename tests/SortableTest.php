<?php

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Schema\Blueprint;
use PHPUnit\Framework\TestCase;
use Thunderwolf\EloquentSortable\SortableException;

class SortableTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $schema = Manager::schema();

        $schema->dropIfExists('tasks');
        $schema->dropIfExists('single-scoped-tasks');
        $schema->dropIfExists('single-scoped-users');
        $schema->dropIfExists('multi-scoped-tasks');
        $schema->dropIfExists('multi-scoped-users');
        $schema->dropIfExists('multi-scoped-groups');

        Manager::connection()->disableQueryLog();

        $schema->create('tasks', function (Blueprint $table1) {
            $table1->increments('id');
            $table1->string('title');
            $table1->createSortable([]);
        });

        $schema->create('single-scoped-tasks', function (Blueprint $table2) {
            $table2->increments('id');
            $table2->unsignedInteger('single_scoped_user_id');
            $table2->string('title');
            $table2->createSortable(['use_scope' => true, 'scope_columns' => ['single_scoped_user_id']]);
        });

        $schema->create('single-scoped-users', function (Blueprint $table3) {
            $table3->increments('id');
            $table3->string('username');
        });

        $schema->create('multi-scoped-tasks', function (Blueprint $table4) {
            $table4->increments('id');
            $table4->unsignedInteger('multi_scoped_user_id');
            $table4->unsignedInteger('multi_scoped_group_id');
            $table4->string('title');
            $table4->createSortable(['use_scope' => true, 'scope_columns' => ['multi_scoped_user_id', 'multi_scoped_group_id']]);
        });

        $schema->create('multi-scoped-users', function (Blueprint $table5) {
            $table5->increments('id');
            $table5->string('username');
        });

        $schema->create('multi-scoped-groups', function (Blueprint $table6) {
            $table6->increments('id');
            $table6->string('name');
        });

        Manager::connection()->enableQueryLog();
    }

    public function setUp(): void
    {
        $tasks_data = include __DIR__ . '/data/tasks.php';
        Manager::table('tasks')->insert($tasks_data);

        $single_scoped_users_data = include __DIR__ . '/data/single-scoped-users.php';
        Manager::table('single-scoped-users')->insert($single_scoped_users_data);

        $multi_scoped_users_data = include __DIR__ . '/data/multi-scoped-users.php';
        Manager::table('multi-scoped-users')->insert($multi_scoped_users_data);

        $multi_scoped_groups_data = include __DIR__ . '/data/multi-scoped-groups.php';
        Manager::table('multi-scoped-groups')->insert($multi_scoped_groups_data);

        Manager::connection()->flushQueryLog();

        date_default_timezone_set('Europe/Warsaw');
    }

    public function tearDown(): void
    {
        Manager::table('tasks')->truncate();
        Manager::table('single-scoped-tasks')->truncate();
        Manager::table('single-scoped-users')->truncate();
        Manager::table('multi-scoped-tasks')->truncate();
        Manager::table('multi-scoped-users')->truncate();
        Manager::table('multi-scoped-groups')->truncate();
    }

    public function testCreateTasks()
    {
        Manager::table('tasks')->truncate();
        $t1 = new Task();
        $t1->setAttribute('title', 'Wash the dishes');
        $t1->save();

        $this->assertEquals(1, $t1->getSortableRank());

        $t2 = new Task();
        $t2->setAttribute('title', 'Do the laundry');
        $t2->save();

        $this->assertEquals(2, $t2->getSortableRank());

        $t3 = new Task();
        $t3->setAttribute('title', 'Rest a little');
        $t3->save();

        $this->assertEquals(3, $t3->getSortableRank());

        $this->assertEquals(false, $t2->isFirst());
        $this->assertEquals(false, $t2->isLast());
        $this->assertEquals(2, $t2->getSortableRank());
    }

    public function testTraverse()
    {
        $firstTask = Task::query()->findOneByRank(1); // $t1
        $this->assertEquals('Wash the dishes', $firstTask->getAttribute('title'));

        $secondTask = $firstTask->getNext();      // $t2
        $this->assertEquals('Do the laundry', $secondTask->getAttribute('title'));

        $lastTask = $secondTask->getNext();       // $t3
        $this->assertEquals('Rest a little', $lastTask->getAttribute('title'));

        $secondTask = $lastTask->getPrevious();   // $t2
        $this->assertEquals('Do the laundry', $secondTask->getAttribute('title'));

        $allTasks = Task::query()->findList();
        $this->assertInstanceOf(Collection::class, $allTasks);
        $expected = ['Wash the dishes', 'Do the laundry', 'Rest a little'];
        foreach ($allTasks as $task) {
            $this->assertEquals(array_shift($expected), $task->getAttribute('title'));
        }
        // => collection($t1, $t2, $t3)

        $allTasksInReverseOrder = Task::query()->orderByRank('desc')->get();
        $this->assertInstanceOf(Collection::class, $allTasksInReverseOrder);
        $expected = ['Rest a little', 'Do the laundry', 'Wash the dishes'];
        foreach ($allTasksInReverseOrder as $task) {
            $this->assertEquals(array_shift($expected), $task->getAttribute('title'));
        }
        // => collection($t3, $t2, $t2)
    }

    /**
     * @depends testTraverse
     */
    public function testManipulatingObjectsInAList()
    {
        $t1 = Task::query()->findOneByRank(1);
        $this->assertEquals('Wash the dishes', $t1->getAttribute('title'));

        $t2 = Task::query()->findOneByRank(2);
        $this->assertEquals('Do the laundry', $t2->getAttribute('title'));

        $t2->moveToTop();
        $expected = ['Do the laundry', 'Wash the dishes', 'Rest a little'];
        $allTasks = Task::query()->findList();
        $this->assertInstanceOf(Collection::class, $allTasks);
        foreach ($allTasks as $key => $task) {
            $this->assertEquals(array_shift($expected), $task->getAttribute('title'));
            $this->assertEquals($key+1, $task->getSortableRank());
        }

        $t2->moveToBottom();
        $expected = ['Wash the dishes', 'Rest a little', 'Do the laundry'];
        $allTasks = Task::query()->findList();
        $this->assertInstanceOf(Collection::class, $allTasks);
        foreach ($allTasks as $key => $task) {
            $this->assertEquals(array_shift($expected), $task->getAttribute('title'));
            $this->assertEquals($key+1, $task->getSortableRank());
        }

        $t2->moveUp();
        $expected = ['Wash the dishes', 'Do the laundry', 'Rest a little'];
        $allTasks = Task::query()->findList();
        $this->assertInstanceOf(Collection::class, $allTasks);
        foreach ($allTasks as $key => $task) {
            $this->assertEquals(array_shift($expected), $task->getAttribute('title'));
            $this->assertEquals($key+1, $task->getSortableRank());
        }

        $t2->swapWith($t1);
        $expected = ['Do the laundry', 'Wash the dishes', 'Rest a little'];
        $allTasks = Task::query()->findList();
        $this->assertInstanceOf(Collection::class, $allTasks);
        foreach ($allTasks as $key => $task) {
            $this->assertEquals(array_shift($expected), $task->getAttribute('title'));
            $this->assertEquals($key+1, $task->getSortableRank());
        }

        $t2->moveToRank(3);
        $expected = ['Wash the dishes', 'Rest a little', 'Do the laundry'];
        $allTasks = Task::query()->findList();
        $this->assertInstanceOf(Collection::class, $allTasks);
        foreach ($allTasks as $key => $task) {
            $this->assertEquals(array_shift($expected), $task->getAttribute('title'));
            $this->assertEquals($key+1, $task->getSortableRank());
        }

        $t2->moveToRank(2);
        $expected = ['Wash the dishes', 'Do the laundry', 'Rest a little'];
        $allTasks = Task::query()->findList();
        $this->assertInstanceOf(Collection::class, $allTasks);
        foreach ($allTasks as $key => $task) {
            $this->assertEquals(array_shift($expected), $task->getAttribute('title'));
            $this->assertEquals($key+1, $task->getSortableRank());
        }
    }

    public function testAddAndRemove()
    {
        $t4 = new Task();
        $t4->setAttribute('title', 'Clean windows');
        $t4->insertAtRank(2);
        $t4->save();
        $expected = ['Wash the dishes', 'Clean windows', 'Do the laundry', 'Rest a little'];
        $allTasks = Task::query()->findList();
        $this->assertInstanceOf(Collection::class, $allTasks);
        foreach ($allTasks as $key => $task) {
            $this->assertEquals(array_shift($expected), $task->getAttribute('title'));
            $this->assertEquals($key+1, $task->getSortableRank());
        }

        $t4->delete();
        $expected = ['Wash the dishes', 'Do the laundry', 'Rest a little'];
        $allTasks = Task::query()->findList();
        $this->assertInstanceOf(Collection::class, $allTasks);
        foreach ($allTasks as $key => $task) {
            $this->assertEquals(array_shift($expected), $task->getAttribute('title'));
            $this->assertEquals($key+1, $task->getSortableRank());
        }
    }

    /**
     * @throws SortableException
     */
    public function testMultipleListSingleScopedSimple()
    {
        Manager::table('single-scoped-tasks')->truncate();

        $paul = SingleScopedUser::query()->find(1);
        $this->assertEquals('paul', $paul->getAttribute('username'));

        $john = SingleScopedUser::query()->find(2);
        $this->assertEquals('john', $john->getAttribute('username'));

        $t1 = new SingleScopedTask();
        $t1->setAttribute('title', 'Wash the dishes');
        $t1->setSortableScope('single_scoped_user_id', $paul->getKey());
        $t1->save();
        $this->assertEquals(1, $t1->getSortableRank());

        $t2 = new SingleScopedTask();
        $t2->setAttribute('title', 'Do the laundry');
        $t2->setSortableScope('single_scoped_user_id', $paul->getKey());
        $t2->save();
        $this->assertEquals(2, $t2->getSortableRank());

        $t3 = new SingleScopedTask();
        $t3->setAttribute('title', 'Rest a little');
        $t3->setSortableScope('single_scoped_user_id', $john->getKey());
        $t3->save();
        $this->assertEquals(1, $t3->getSortableRank());

        $firstPaulTask = SingleScopedTask::query()->findOneByRank(1, ['single_scoped_user_id' => $paul->getKey()]); // $t1
        $this->assertEquals('Wash the dishes', $firstPaulTask->getAttribute('title'));

        $lastPaulTask = $firstPaulTask->getNext();      // $t2
        $this->assertEquals('Do the laundry', $lastPaulTask->getAttribute('title'));

        $firstJohnTask = SingleScopedTask::query()->findOneByRank(1, ['single_scoped_user_id' => $john->getKey()]); // $t3
        $this->assertEquals('Rest a little', $firstJohnTask->getAttribute('title'));

        $allPaulsTasks = SingleScopedTask::query()->inList(['single_scoped_user_id' => $paul->getKey()])->get();
        $expected = ['Wash the dishes', 'Do the laundry'];
        $this->assertInstanceOf(Collection::class, $allPaulsTasks);
        foreach ($allPaulsTasks as $key => $task) {
            $this->assertEquals(array_shift($expected), $task->getAttribute('title'));
            $this->assertEquals($key+1, $task->getSortableRank());
        }
    }

    public function testMultipleListSingleScopedRelation()
    {
        Manager::table('single-scoped-tasks')->truncate();

        $paul = SingleScopedUser::query()->find(1);
        $this->assertEquals('paul', $paul->getAttribute('username'));

        $john = SingleScopedUser::query()->find(2);
        $this->assertEquals('john', $john->getAttribute('username'));

        $t1 = new SingleScopedTask(['title' => 'Wash the dishes']);
        $paul->tasks()->save($t1);
        $this->assertEquals(1, $t1->getSortableRank());

        $t2 = new SingleScopedTask(['title' => 'Do the laundry']);
        $paul->tasks()->save($t2);
        $this->assertEquals(2, $t2->getSortableRank());

        $t3 = new SingleScopedTask(['title' => 'Rest a little']);
        $john->tasks()->save($t3);
        $this->assertEquals(1, $t3->getSortableRank());

        $firstPaulTask = SingleScopedTask::query()->findOneByRank(1, ['single_scoped_user_id' => $paul->getKey()]); // $t1
        $this->assertEquals('Wash the dishes', $firstPaulTask->getAttribute('title'));

        $lastPaulTask = $firstPaulTask->getNext();      // $t2
        $this->assertEquals('Do the laundry', $lastPaulTask->getAttribute('title'));

        $firstJohnTask = SingleScopedTask::query()->findOneByRank(1, ['single_scoped_user_id' => $john->getKey()]); // $t3
        $this->assertEquals('Rest a little', $firstJohnTask->getAttribute('title'));

        $allPaulsTasks = SingleScopedTask::query()->inList(['single_scoped_user_id' => $paul->getKey()])->get();
        $expected = ['Wash the dishes', 'Do the laundry'];
        $this->assertInstanceOf(Collection::class, $allPaulsTasks);
        foreach ($allPaulsTasks as $key => $task) {
            $this->assertEquals(array_shift($expected), $task->getAttribute('title'));
            $this->assertEquals($key+1, $task->getSortableRank());
        }
    }

    public function testMultipleListMultiScopedSimple()
    {
        Manager::table('multi-scoped-tasks')->truncate();

        $paul = MultiScopedUser::query()->find(1);
        $this->assertEquals('paul', $paul->getAttribute('username'));

        $john = MultiScopedUser::query()->find(2);
        $this->assertEquals('john', $john->getAttribute('username'));

        $adminGroup = MultiScopedGroup::query()->find(1);
        $this->assertEquals('admin', $adminGroup->getAttribute('name'));

        $userGroup = MultiScopedGroup::query()->find(2);
        $this->assertEquals('user', $userGroup->getAttribute('name'));

        $t1 = new MultiScopedTask();
        $t1->setAttribute('title', 'Create permissions');
        $t1->setSortableScope('multi_scoped_user_id', $paul->getKey());
        $t1->setSortableScope('multi_scoped_group_id', $adminGroup->getKey());
        $t1->save();
        $this->assertEquals(1, $t1->getSortableRank());

        $t2 = new MultiScopedTask();
        $t2->setAttribute('title', 'Grant permissions to users');
        $t2->setSortableScope('multi_scoped_user_id', $paul->getKey());
        $t2->setSortableScope('multi_scoped_group_id', $adminGroup->getKey());
        $t2->save();
        $this->assertEquals(2, $t2->getSortableRank());

        $t3 = new MultiScopedTask();
        $t3->setAttribute('title', 'Install servers');
        $t3->setSortableScope('multi_scoped_user_id', $john->getKey());
        $t3->setSortableScope('multi_scoped_group_id', $adminGroup->getKey());
        $t3->save();
        $this->assertEquals(1, $t3->getSortableRank());  // 1, because John has his own task list inside the admin-group

        $t4 = new MultiScopedTask();
        $t4->setAttribute('title', 'Manage content');
        $t4->setSortableScope('multi_scoped_user_id', $john->getKey());
        $t4->setSortableScope('multi_scoped_group_id', $userGroup->getKey());
        $t4->save();
        $this->assertEquals(1, $t4->getSortableRank());  // 1, because John has his own task list inside the user-group

        // $t1
        $firstPaulAdminTask = MultiScopedTask::query()->findOneByRank(
            1,
            ['multi_scoped_user_id' => $paul->getKey(), 'multi_scoped_group_id' => $adminGroup->getKey()]
        );
        $this->assertEquals('Create permissions', $firstPaulAdminTask->getAttribute('title'));

        // $t2
        $lastPaulTask = $firstPaulAdminTask->getNext();
        $this->assertEquals('Grant permissions to users', $lastPaulTask->getAttribute('title'));

        // $t4
        $firstJohnUserTask = MultiScopedTask::query()->findOneByRank(
            1,
            ['multi_scoped_user_id' => $john->getKey(), 'multi_scoped_group_id' => $userGroup->getKey()]
        );
        $this->assertEquals('Manage content', $firstJohnUserTask->getAttribute('title'));

        $allJohnsUserTasks = MultiScopedTask::query()
            ->inList(['multi_scoped_user_id' => $john->getKey(), 'multi_scoped_group_id' => $userGroup->getKey()])
            ->get();
        $expected = ['Manage content'];
        $this->assertInstanceOf(Collection::class, $allJohnsUserTasks);
        foreach ($allJohnsUserTasks as $key => $task) {
            $this->assertEquals(array_shift($expected), $task->getAttribute('title'));
            $this->assertEquals($key+1, $task->getSortableRank());
        }
    }

    public function testMultipleListMultiScopedRelation()
    {
        Manager::table('multi-scoped-tasks')->truncate();

        $paul = MultiScopedUser::query()->find(1);
        $this->assertEquals('paul', $paul->getAttribute('username'));

        $john = MultiScopedUser::query()->find(2);
        $this->assertEquals('john', $john->getAttribute('username'));

        $adminGroup = MultiScopedGroup::query()->find(1);
        $this->assertEquals('admin', $adminGroup->getAttribute('name'));

        $userGroup = MultiScopedGroup::query()->find(2);
        $this->assertEquals('user', $userGroup->getAttribute('name'));

        $t1 = new MultiScopedTask(['title' => 'Create permissions']);
        $t1->user()->associate($paul);
        $t1->group()->associate($adminGroup);
        $t1->save();
        $this->assertEquals(1, $t1->getSortableRank());

        $t2 = new MultiScopedTask(['title' => 'Grant permissions to users']);
        $t2->user()->associate($paul);
        $t2->group()->associate($adminGroup);
        $t2->save();
        $this->assertEquals(2, $t2->getSortableRank());

        $t3 = new MultiScopedTask(['title' => 'Install servers']);
        $t3->user()->associate($john);
        $t3->group()->associate($adminGroup);
        $t3->save();
        $this->assertEquals(1, $t3->getSortableRank());  // 1, because John has his own task list inside the admin-group

        $t4 = new MultiScopedTask(['title' => 'Manage content']);
        $t4->user()->associate($john);
        $t4->group()->associate($userGroup);
        $t4->save();
        $this->assertEquals(1, $t4->getSortableRank());  // 1, because John has his own task list inside the user-group

        // $t1
        $firstPaulAdminTask = MultiScopedTask::query()->findOneByRank(
            1,
            ['multi_scoped_user_id' => $paul->getKey(), 'multi_scoped_group_id' => $adminGroup->getKey()]
        );
        $this->assertEquals('Create permissions', $firstPaulAdminTask->getAttribute('title'));

        // $t2
        $lastPaulTask = $firstPaulAdminTask->getNext();
        $this->assertEquals('Grant permissions to users', $lastPaulTask->getAttribute('title'));

        // $t4
        $firstJohnUserTask = MultiScopedTask::query()->findOneByRank(
            1,
            ['multi_scoped_user_id' => $john->getKey(), 'multi_scoped_group_id' => $userGroup->getKey()]
        );
        $this->assertEquals('Manage content', $firstJohnUserTask->getAttribute('title'));

        $allJohnsUserTasks = MultiScopedTask::query()
            ->inList(['multi_scoped_user_id' => $john->getKey(), 'multi_scoped_group_id' => $userGroup->getKey()])
            ->get();
        $expected = ['Manage content'];
        $this->assertInstanceOf(Collection::class, $allJohnsUserTasks);
        foreach ($allJohnsUserTasks as $key => $task) {
            $this->assertEquals(array_shift($expected), $task->getAttribute('title'));
            $this->assertEquals($key+1, $task->getSortableRank());
        }
    }
}