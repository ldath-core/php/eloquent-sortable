<?php

use Illuminate\Database\Capsule\Manager;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Thunderwolf\EloquentSortable\SortableServiceProvider;

include __DIR__.'/../vendor/autoload.php';

$container = new Container();
$provider = new SortableServiceProvider($container);
$provider->register();

$capsule = new Manager($container);
$capsule->addConnection([ 'driver' => 'sqlite', 'database' => ':memory:', 'prefix' => 'prfx_' ]);
$capsule->setEventDispatcher(new Dispatcher);
$capsule->setAsGlobal();
$capsule->bootEloquent();

include __DIR__ . '/models/Task.php';
include __DIR__ . '/models/SingleScopedUser.php';
include __DIR__ . '/models/SingleScopedTask.php';
include __DIR__ . '/models/MultiScopedUser.php';
include __DIR__ . '/models/MultiScopedGroup.php';
include __DIR__ . '/models/MultiScopedTask.php';
