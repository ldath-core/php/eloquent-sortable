<?php

return [
    ['id' => 1, 'title' => 'Wash the dishes', 'sortable_rank' => 1],
    ['id' => 2, 'title' => 'Do the laundry', 'sortable_rank' => 2],
    ['id' => 3, 'title' => 'Rest a little', 'sortable_rank' => 3],
];