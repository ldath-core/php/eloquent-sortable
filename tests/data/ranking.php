<?php

return [
    ['id' => 1, 'title' => 'Team A', 'sortable_rank' => 2],
    ['id' => 2, 'title' => 'Team B', 'sortable_rank' => 1],
    ['id' => 3, 'title' => 'Team C', 'sortable_rank' => 6],
    ['id' => 4, 'title' => 'Team D', 'sortable_rank' => 4],
    ['id' => 5, 'title' => 'Team E', 'sortable_rank' => 3],
    ['id' => 6, 'title' => 'Team F', 'sortable_rank' => 5],
];