<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class SingleScopedUser extends Model
{
    protected $table = 'single-scoped-users';

    protected $fillable = ['username'];

    public $timestamps = false;

    public function tasks(): HasMany
    {
        return $this->hasMany(SingleScopedTask::class);
    }
}