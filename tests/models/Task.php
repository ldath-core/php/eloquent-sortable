<?php

use Illuminate\Database\Eloquent\Model;
use Thunderwolf\EloquentSortable\Sortable;

class Task extends Model
{
    use Sortable;

    protected $table = 'tasks';

    protected $fillable = ['title'];

    public $timestamps = false;

    public static function sortable(): array
    {
        return [];
    }
}