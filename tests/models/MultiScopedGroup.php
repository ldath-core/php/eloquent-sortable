<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class MultiScopedGroup extends Model
{
    protected $table = 'multi-scoped-groups';

    protected $fillable = ['name'];

    public $timestamps = false;

    public function tasks(): HasMany
    {
        return $this->hasMany(MultiScopedTask::class);
    }
}