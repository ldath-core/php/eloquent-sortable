<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Thunderwolf\EloquentSortable\Sortable;

class SingleScopedTask extends Model
{
    use Sortable;

    protected $table = 'single-scoped-tasks';

    protected $fillable = ['title'];

    public $timestamps = false;

    public static function sortable(): array
    {
        return ['use_scope' => true, 'scope_columns' => ['single_scoped_user_id']];
    }

    /**
     * When planning to use `Inserting Related Models` way of set user id we need to override setter - we are using 8.x
     *
     * @param $value
     * @return void
     */
    public function setSingleScopedUserIdAttribute($value)
    {
        $this->oldScope['single_scoped_user_id'] = $this->attributes['single_scoped_user_id']??null;
        $this->attributes['single_scoped_user_id'] = $value;
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(SingleScopedUser::class, 'single_scoped_user_id');
    }
}