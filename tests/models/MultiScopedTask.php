<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Thunderwolf\EloquentSortable\Sortable;

class MultiScopedTask extends Model
{
    use Sortable;

    protected $table = 'multi-scoped-tasks';

    protected $fillable = ['title'];

    public $timestamps = false;

    public static function sortable(): array
    {
        return ['use_scope' => true, 'scope_columns' => ['multi_scoped_user_id', 'multi_scoped_group_id']];
    }

    /**
     * When planning to use `Inserting Related Models` way of set user id we need to override setter - we are using 8.x
     *
     * @param $value
     * @return void
     */
    public function setMultiScopedUserIdAttribute($value)
    {
        $this->oldScope['multi_scoped_user_id'] = $this->attributes['multi_scoped_user_id']??null;
        $this->attributes['multi_scoped_user_id'] = $value;
    }

    /**
     * When planning to use `Inserting Related Models` way of set user id we need to override setter - we are using 8.x
     *
     * @param $value
     * @return void
     */
    public function setMultiScopedGroupIdAttribute($value)
    {
        $this->oldScope['multi_scoped_group_id'] = $this->attributes['multi_scoped_group_id']??null;
        $this->attributes['multi_scoped_group_id'] = $value;
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(MultiScopedUser::class, 'multi_scoped_user_id');
    }

    public function group(): BelongsTo
    {
        return $this->belongsTo(MultiScopedGroup::class, 'multi_scoped_group_id');
    }

}