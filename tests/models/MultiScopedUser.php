<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class MultiScopedUser extends Model
{
    protected $table = 'multi-scoped-users';

    protected $fillable = ['username'];

    public $timestamps = false;

    public function tasks(): HasMany
    {
        return $this->hasMany(MultiScopedTask::class);
    }
}